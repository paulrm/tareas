<!DOCTYPE html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<title>Bootstrap layout</title>
	<script src="js/dhtmlxgantt.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="css/dhtmlxgantt.css" type="text/css" media="screen" title="no title" charset="utf-8">
   <script src="js/dhtmlxgantt_marker.js"></script> 



	<script src="js/jquery-1.11.1.min.js" type="text/javascript" charset="utf-8"></script>

	<link rel="stylesheet" href="bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<script src="bootstrap/3.2.0/js/bootstrap.min.js"></script>

	<style type="text/css">
		html, body{ height:100%; padding:0px; margin:0px; }
		.weekend{ background: #f4f7f4 !important;}
		.gantt_selected .weekend{ background:#FFF3A1 !important; }
		.well {
			text-align: right;
		}
		@media (max-width: 991px) {
			.nav-stacked>li{ float: left;}
		}
		.container-fluid .row {
			margin-bottom: 10px;
		}
		.container-fluid .gantt_wrapper {
			height: 700px;
			width: 100%;
		}
		.gantt_container {
			border-radius: 4px;
		}
		.gantt_grid_scale { background-color: transparent; }
		.gantt_hor_scroll { margin-bottom: 1px; }
	</style>
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="navbar navbar-inverse">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">Basic Gantt Chart</a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Project <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Project information</a></li>
									<li><a href="#">Custom fields</a></li>
									<li><a href="#">Change working time</a></li>
									<li class="divider"></li>
									<li><a href="#">Export</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Task <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Add task</a></li>
									<li><a href="#">Add milestone</a></li>
									<li class="divider"></li>
									<li><a href="#">Summary</a></li>
								</ul>
							</li>
							<li><a href="#">Team</a></li>
							<li><a href="#">Format</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div>
<input type="radio" id="scale1" name="scale" value="1" /><label for="scale1">Day scale</label>
<input type="radio" id="scale2" name="scale" value="2" /><label for="scale2">Week scale</label>
<input type="radio" id="scale3" name="scale" value="3" /><label for="scale3">Month scale</label>
<input type="radio" id="scale4" name="scale" value="4" checked /><label for="scale4">Year scale</label>
</div>
	</div>
	</div>
	<div class="row">
		<div class="col-md-2 col-md-push-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Gantt info</h3>
				</div>
				<div class="panel-body">
					<ul class="nav nav-pills nav-stacked" id="gantt_info">
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-10 col-md-pull-2">
			<div class="gantt_wrapper panel" id="gantt_here"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="well">
				<div>
					<a class="logo" title="DHTMLX - JavaScript Web App Framework &amp; UI Widgets" href="http://dhtmlx.com/docs/products/dhtmlxGantt/">&copy; DHTMLX</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">






	var demo_tasks = {
		data:[
			{"id":1, "text":"Office itinerancy", "type":gantt.config.types.project, "order":"10", progress: 0.4, open: false},

			{"id":2, "text":"Office facing", "type":gantt.config.types.project, "start_date":"02-04-2013", "duration":"8", "order":"10", progress: 0.6, "parent":"1", open: true},
			{"id":3, "text":"Furniture installation", "type":gantt.config.types.project, "start_date":"11-04-2013", "duration":"8", "order":"20", "parent":"1", progress: 0.6, open: true},
			{"id":4, "text":"The employee relocation", "type":gantt.config.types.project, "start_date":"13-04-2013", "duration":"6", "order":"30", "parent":"1", progress: 0.5, open: true},

			{"id":5, "text":"Interior office", "start_date":"02-04-2013", "duration":"7", "order":"3", "parent":"2", progress: 0.6, open: true},
			{"id":6, "text":"Air conditioners check", "start_date":"03-04-2013", "duration":"7", "order":"3", "parent":"2", progress: 0.6, open: true},
			{"id":7, "text":"Workplaces preparation", "start_date":"11-04-2013", "duration":"8", "order":"3", "parent":"3", progress: 0.6, open: true},
			{"id":8, "text":"Preparing workplaces", "start_date":"14-04-2013", "duration":"5", "order":"3", "parent":"4", progress: 0.5, open: true},
			{"id":9, "text":"Workplaces importation", "start_date":"14-04-2013", "duration":"4", "order":"3", "parent":"4", progress: 0.5, open: true},
			{"id":10, "text":"Workplaces exportation", "start_date":"14-04-2013", "duration":"3", "order":"3", "parent":"4", progress: 0.5, open: true},

			{"id":11, "text":"Product launch", "type":gantt.config.types.project, "order":"5", progress: 0.6, open: true},

			{"id":12, "text":"Perform Initial testing", "start_date":"03-04-2013", "duration":"5", "order":"3", "parent":"11", progress: 1, open: true},
			{"id":13, "text":"Development", "type":gantt.config.types.project, "start_date":"02-04-2013", "duration":"7", "order":"3", "parent":"11", progress: 0.5, open: true},
			{"id":14, "text":"Analysis", "start_date":"02-04-2013", "duration":"6", "order":"3", "parent":"11", progress: 0.8, open: true},
			{"id":15, "text":"Design", "type":gantt.config.types.project, "start_date":"02-04-2013", "duration":"5", "order":"3", "parent":"11", progress: 0.2, open: false},
			{"id":16, "text":"Documentation creation", "start_date":"02-04-2013", "duration":"7", "order":"3", "parent":"11", progress: 0, open: true},

			{"id":17, "text":"Develop System", "start_date":"03-04-2013", "duration":"2", "order":"3", "parent":"13", progress: 1, open: true},

			{"id":25, "text":"Beta Release", "start_date":"06-04-2013", "order":"3","type":gantt.config.types.milestone, "parent":"13", progress: 0, open: true},

			{"id":18, "text":"Integrate System", "start_date":"08-04-2013", "duration":"2", "order":"3", "parent":"13", progress: 0.8, open: true},
			{"id":19, "text":"Test", "start_date":"10-04-2013", "duration":"4", "order":"3", "parent":"13", progress: 0.2, open: true},
			{"id":20, "text":"Marketing", "start_date":"10-04-2013", "duration":"4", "order":"3", "parent":"13", progress: 0, open: true},

			{"id":21, "text":"Design database", "start_date":"03-04-2013", "duration":"4", "order":"3", "parent":"15", progress: 0.5, open: true},
			{"id":22, "text":"Software design", "start_date":"03-04-2013", "duration":"4", "order":"3", "parent":"15", progress: 0.1, open: true},
			{"id":23, "text":"Interface setup", "start_date":"03-04-2013", "duration":"5", "order":"3", "parent":"15", progress: 0, open: true},
			{"id":24, "text":"Release v1.0", "start_date":"15-04-2013", "order":"3","type":gantt.config.types.milestone, "parent":"11", progress: 0, open: true}
		],
		links:[
			{id:"1",source:"1",target:"2",type:"1"},

			{id:"2",source:"2",target:"3",type:"0"},
			{id:"3",source:"3",target:"4",type:"0"},
			{id:"4",source:"2",target:"5",type:"2"},
			{id:"5",source:"2",target:"6",type:"2"},
			{id:"6",source:"3",target:"7",type:"2"},
			{id:"7",source:"4",target:"8",type:"2"},
			{id:"8",source:"4",target:"9",type:"2"},
			{id:"9",source:"4",target:"10",type:"2"},

			{id:"10",source:"11",target:"12",type:"1"},
			{id:"11",source:"11",target:"13",type:"1"},
			{id:"12",source:"11",target:"14",type:"1"},
			{id:"13",source:"11",target:"15",type:"1"},
			{id:"14",source:"11",target:"16",type:"1"},

			{id:"15",source:"13",target:"17",type:"1"},
			{id:"16",source:"17",target:"25",type:"0"},
			{id:"23",source:"25",target:"18",type:"0"},
			{id:"17",source:"18",target:"19",type:"0"},
			{id:"18",source:"19",target:"20",type:"0"},
			{id:"19",source:"15",target:"21",type:"2"},
			{id:"20",source:"15",target:"22",type:"2"},
			{id:"21",source:"15",target:"23",type:"2"},
			{id:"22",source:"13",target:"24",type:"0"}
		]
	};

	var getListItemHTML = function (type, count, active) {
		return '<li'+(active?' class="active"':'')+'><a href="#">'+type+'s <span class="badge">'+count+'</span></a></li>';
	};

	var updateInfo = function () {
		var state = gantt.getState(),
				tasks = gantt.getTaskByTime(state.min_date, state.max_date),
				types = gantt.config.types,
				result = {},
				html = "",
				active = false;

		// get available types
		for (var t in types) {
			result[types[t]] = 0;
		}
		// sort tasks by type
		for (var i=0, l=tasks.length; i<l; i++) {
			if (tasks[i].type && result[tasks[i].type] != "undefined")
				result[tasks[i].type] += 1;
			else
				result[types.task] += 1;
		}
		// render list items for each type
		for (var j in result) {
			if (j == types.task)
				active = true;
			else
				active = false;
			html += getListItemHTML(j, result[j], active);
		}

		document.getElementById("gantt_info").innerHTML = html;
	};

	gantt.templates.scale_cell_class = function(date){
		if(date.getDay()==0||date.getDay()==6){
			return "weekend";
		}
	};
	gantt.templates.task_cell_class = function(item,date){
		if(date.getDay()==0||date.getDay()==6){
			return "weekend" ;
		}
	};

	gantt.templates.rightside_text = function(start, end, task){
		if(task.type == gantt.config.types.milestone){
			return task.text;
		}
		return "";
	};

/*
	gantt.config.columns = [
		{name:"text",       label:"Task name",  width:"*", tree:true },
		{name:"start_time",   label:"Start time",  template:function(obj){
			return gantt.templates.date_grid(obj.start_date);
		}, align: "center", width:60 },
		{name:"duration",   label:"Duration", align:"center", width:60},
		{name:"add",        label:"",           width:44 }
	];

	gantt.config.grid_width = 450;
	gantt.config.date_grid = "%F %d";
	gantt.config.subscales = [
		{ unit:"week", step:1, date:"Week #%W"}
	];
*/
    gantt.config.columns = [
        {name:"text", label:"Task name", tree:true, width:200 },
        {name:"progress", label:"Progress", width:80, align: "center",
            template: function(item) {
                if (item.progress >= 1)
                    return "Complete";
                if (item.progress == 0)
                    return "Not started";
                return Math.round(item.progress*100) + "%";
            }
        },
        {name:"responsable", label:"responsable", align: "center", width:100,
            template: function(item) {
                if (!item.users) return "Nobody";
                return item.users.join(", ");
            }
        },
        {name:"ejecutor", label:"ejecutor", align: "center", width:100,
            template: function(item) {
                if (!item.users) return "Nobody";
                return item.users.join(", ");
            }
        }
    ];

gantt.date.quarter_start = function(date){ 
   gantt.date.month_start(date);
   var m = date.getMonth(),
      res_month;
 
   if(m >= 9){
      res_month = 9;
   }else if(m >= 6){
      res_month = 6;
   }else if(m >= 3){
      res_month = 3;
   }else{
      res_month = 0;
   }
 
   date.setMonth(res_month);
   return date;
};
gantt.date.add_quarter = function(date, inc){ 
   return gantt.date.add(date, inc*3, "month");
};

function quarterLabel(date){
    var month = date.getMonth();
    var q_num;
 
    if(month >= 9){ 
        q_num = 4;
    }else if(month >= 6){
        q_num = 3;
    }else if(month >= 3){
        q_num = 2;
    }else{
        q_num = 1;
    }
    return "Q" + q_num;
}
 
gantt.config.subscales = [
    {unit:"quarter", step:1, template:quarterLabel},
    {unit:"month", step:1, date:"%M" }
];

gantt.config.scale_height = 54;

	gantt.attachEvent("onAfterTaskAdd", function(id,item){
		updateInfo();
	});
	gantt.attachEvent("onAfterTaskDelete", function(id,item){
		updateInfo();
	});
var markerId = gantt.addMarker({
    start_date: new Date(2018,01,03),
    css: "today", //a CSS class applied to the marker
    text: "Now", //the marker title
    title:"Title" // the marker's tooltip
});

var markerId1 = gantt.addMarker({
    start_date: new Date(2018,01,18),
    css: "today", //a CSS class applied to the marker
    text: "Abr-06", //the marker title
    title:"Title" // the marker's tooltip
});


/*
gantt.attachEvent("onAfterTaskUpdate", function(id,item){
    //any custom logic here
    var task = gantt.getTask(id);
    task.text = "Overrride";
    task.start_date = "03-06-2013";
    //gantt.updateTask(id);
    //task.start_date = Date(2013,03,06);
return true;
});
*/


gantt.config.fit_tasks = true; 


	function setScaleConfig(value){
		switch (value) {
			case "1":
				gantt.config.scale_unit = "day";
				gantt.config.step = 1;
				gantt.config.date_scale = "%d %M";
				gantt.config.subscales = [];
				gantt.config.scale_height = 27;
				gantt.templates.date_scale = null;
				break;
			case "2":
				var weekScaleTemplate = function(date){
					var dateToStr = gantt.date.date_to_str("%d %M");
					var endDate = gantt.date.add(gantt.date.add(date, 1, "week"), -1, "day");
					return dateToStr(date) + " - " + dateToStr(endDate);
				};

				gantt.config.scale_unit = "week";
				gantt.config.step = 1;
				gantt.templates.date_scale = weekScaleTemplate;
				gantt.config.subscales = [
					{unit:"day", step:1, date:"%D" }
				];
				gantt.config.scale_height = 50;
				break;
			case "3":
				gantt.config.scale_unit = "month";
				gantt.config.date_scale = "%F, %Y";
				gantt.config.subscales = [
					{unit:"day", step:1, date:"%j, %D" }
				];
				gantt.config.scale_height = 50;
				gantt.templates.date_scale = null;
				break;
			case "4":
				gantt.config.scale_unit = "year";
				gantt.config.step = 1;
				gantt.config.date_scale = "%Y";
				gantt.config.min_column_width = 50;

				gantt.config.scale_height = 90;
				gantt.templates.date_scale = null;

				
				gantt.config.subscales = [
					{unit:"month", step:1, date:"%M" }
				];
				break;
		}
	}

	setScaleConfig('4');

 


gantt.config.xml_date = "%Y-%m-%d %H:%i:%s";
gantt.init("gantt_here");
gantt.load("http://hal-api-rest.invap.com.ar/data");
	//gantt.parse(demo_tasks);

	updateInfo();

	var func = function(e) {
		e = e || window.event;
		var el = e.target || e.srcElement;
		var value = el.value;
		setScaleConfig(value);
		gantt.render();
	};

	var els = document.getElementsByName("scale");
	for (var i = 0; i < els.length; i++) {
		els[i].onclick = func;
	}

</script>
</body>
