<!DOCTYPE html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<title>Bootstrap layout</title>
	<script src="js/dhtmlxgantt.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="css/dhtmlxgantt.css" type="text/css" media="screen" title="no title" charset="utf-8">

	<script src="js/jquery-1.11.1.min.js" type="text/javascript" charset="utf-8"></script>

	<link rel="stylesheet" href="bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<script src="bootstrap/3.2.0/js/bootstrap.min.js"></script>

	<style type="text/css">
		html, body{ height:100%; padding:0px; margin:0px; }
		.weekend{ background: #f4f7f4 !important;}
		.gantt_selected .weekend{ background:#FFF3A1 !important; }
		.well {
			text-align: right;
		}
		@media (max-width: 991px) {
			.nav-stacked>li{ float: left;}
		}
		.container-fluid .row {
			margin-bottom: 10px;
		}
		.container-fluid .gantt_wrapper {
			height: 700px;
			width: 100%;
		}
		.gantt_container {
			border-radius: 4px;
		}
		.gantt_grid_scale { background-color: transparent; }
		.gantt_hor_scroll { margin-bottom: 1px; }
	</style>
</head>
<body>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="navbar navbar-inverse">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">Basic Gantt Chart</a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Project <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Project information</a></li>
									<li><a href="#">Custom fields</a></li>
									<li><a href="#">Change working time</a></li>
									<li class="divider"></li>
									<li><a href="#">Export</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Task <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Add task</a></li>
									<li><a href="#">Add milestone</a></li>
									<li class="divider"></li>
									<li><a href="#">Summary</a></li>
								</ul>
							</li>
							<li><a href="#">Team</a></li>
							<li><a href="#">Format</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	<!--	<div class="col-md-2 col-md-push-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Gantt info</h3>
				</div>
				<div class="panel-body">
					<ul class="nav nav-pills nav-stacked" id="gantt_info">
					</ul>
				</div>
			</div>
		</div>
        -->
		<div >
			<div class="gantt_wrapper panel" id="gantt_here"></div>
		</div>
	</div>
</div>

<script type="text/javascript">

	var getListItemHTML = function (type, count, active) {
		return '<li'+(active?' class="active"':'')+'><a href="#">'+type+'s <span class="badge">'+count+'</span></a></li>';
	};

	var updateInfo = function () {
		var state = gantt.getState(),
				tasks = gantt.getTaskByTime(state.min_date, state.max_date),
				types = gantt.config.types,
				result = {},
				html = "",
				active = false;

		// get available types
		for (var t in types) {
			result[types[t]] = 0;
		}
		// sort tasks by type
		for (var i=0, l=tasks.length; i<l; i++) {
			if (tasks[i].type && result[tasks[i].type] != "undefined")
				result[tasks[i].type] += 1;
			else
				result[types.task] += 1;
		}
		// render list items for each type
		for (var j in result) {
			if (j == types.task)
				active = true;
			else
				active = false;
			html += getListItemHTML(j, result[j], active);
		}

	};

	gantt.templates.scale_cell_class = function(date){
		if(date.getDay()==0||date.getDay()==6){
			return "weekend";
		}
	};
	gantt.templates.task_cell_class = function(item,date){
		if(date.getDay()==0||date.getDay()==6){
			return "weekend" ;
		}
	};

	gantt.templates.rightside_text = function(start, end, task){
		if(task.type == gantt.config.types.milestone){
			return task.text;
		}
		return "";
	};

	gantt.config.columns = [
		{name:"text",       label:"Task name",  width:"*", tree:true },
		{name:"start_time",   label:"Start time",  template:function(obj){
			return gantt.templates.date_grid(obj.start_date);
		}, align: "center", width:60 },
		{name:"duration",   label:"Duration", align:"center", width:60},
		{name:"add",        label:"",           width:44 }
	];

	gantt.config.grid_width = 390;
	gantt.config.date_grid = "%F %d";
	gantt.config.scale_height  = 60;
	gantt.config.subscales = [
		{ unit:"week", step:1, date:"Week #%W"}
	];

	gantt.attachEvent("onAfterTaskAdd", function(id,item){
		updateInfo();
	});
	gantt.attachEvent("onAfterTaskDelete", function(id,item){
		updateInfo();
	});

gantt.config.xml_date = "%Y-%m-%d %H:%i:%s";
	gantt.init("gantt_here");
        gantt.load("http://hal-api-rest.invap.com.ar/data");
	updateInfo();

        var dp = new gantt.dataProcessor("http://hal-api-rest.invap.com.ar/data");
        dp.init(gantt);
        dp.setTransactionMode("REST");

</script>
</body>
