# Tareas
Prueba de tecnologias de ingreso de datos de tareas y planificacion

##  We have / Pending 
- [x] El componente GANTT presenta informacion desde BackEnd
- [x] Ejecuta un CRUD basico
- [x] Indent / Outdent
- [x] Cambio de escala (probado en un test intermedio pero no el final)
- [x] Cambio de sortOrder desde la UI

## Proximo Sprint (aun no planeado)
- [ ] una tarea debe por asignarse un responsable que sea Grupo, Persona o UnAssigned
- [ ] una tarea debe por asignarse un ejecutor que sea Grupo, Persona o UnAssigned
- [ ] Login x LDAP
- [ ] CRUD Grupos
- [ ] CRUD Personas Relacionado con LDAP

## 2 Review  
- Dado el User lo contrtola contra Grupos
- Obtine la lista de grupos y subgrupos a los que pertence
- Lista las tareas que pertences a los grupos y subgrupos 
- Al crear una tarea, el usuario es el owner y debe asignara a alguno de sus grupos
- Una master task, debe empezar en una InvapPeriodoStartDate y Terminar en una InvapPeriodoEndDate -> una tarea tiene n pediodos
- Datos una master task, cargar el % o HH de los n periodos que comprende
* Que el valor de las subtareas recalcule el valor de la tarea padre
* Que el Pediodo sea fijo x dia aka Periodo INVAP
* la distribucion dentro de la tarea, sera resuelta luego
* columnas adicionales
* ver donde agregar el resposable y el ejecutor


# History


## 2018-02-20 
* hecha la persistencia del sortOrder Fix #25 /Close
* hecha la persistencia de Create Fix #26 /Close

## 2018-02-19 
* Atacando tema de SortOrder 
  * ver: gantt.config.sorta https://docs.dhtmlx.com/gantt/desktop__reodering_tasks.html
* tema de escala Fixed #22 /Close

## 2018-02-16
* re-ordenando proximo sprint - paulrm/tareas%"v0.6"

## 2018-02-14
* Fixed #21 /Close
* Delete de Tasks y Links Fixed #20 /Close

## 2018-02-01
* Re-encarando el sprint

``` 
wget http://hal-api-rest.invap.com.ar/plan 
wget http://hal-api-rest.invap.com.ar/data 
python -m json.tool dataOk.json  > /tmp/dataOk.txt
python -m json.tool dataNotok.json > /tmp/dataNotok.json
```

## 2018-02-09
* ya aplicando algunos cambios desde UI a DB

## 2018-01-18 
* tratando de integrar front-end y back-end, ya se comunican pero el front-end tira un invalid-data

## 2018-01-04 
* 1era version de la creacion de la base con algunos problemas
   * datetime -> date
   * tengo un error en como expecifica la FK en grupos
   * hay un par de constrains con REFERENCE () y al parecer no le gusta

## 2018-01-03
* Agregado del test-pgsql que lee desde la base

## 2018-01-02
* Agregado de DDL

## 2017-12-22
* agregado de Markers, 
* marker dinamico en un update https://docs.dhtmlx.com/gantt/snippet/479c6f37
* :shit: no logro modificar la start_date dentro del evento 
* agregado de scale variable
* agregado de columnas en seccion de tasks

## 2017-12-20
* Agregando prueba estatica de DHTMLX Gantt 
* Agregado prueba dinamica
* Agregado test03

## 2017-12-19
* Empenzado
* Agregado el test minimo de jsTree - v3.2.1
* Agregado el test de Google Graph
* Ordenando la evaluacion de modulos
* Agregado algunos modulos que paso JPF
    * https://github.com/demarchisd/stacked-gantt/
    * https://github.com/jsGanttImproved/jsgantt-improved
    * https://github.com/mbielanczuk/jQuery.Gantt
    * https://github.com/thegrubbsian/jquery.ganttView
    * https://github.com/robicch/jQueryGantt
    * https://github.com/oguzhanoya/jquery-gantt
* Y otros que encontre yo
    * https://www.ag-grid.com
    * http://www.jeasyui.com/tutorial/tree/treegrid2.php
    * http://www.guriddo.net/demo/treegridjs/
* Otra doc intgeresante:
    * https://docs.dhtmlx.com/gantt/desktop__howtostart_php.html
* Siguiente documentacion interesante
 

# Componentes Evaluados

| Modulos Candidatos                                 | Lic             | Tested   | URL                                              |
|----------------------------------------------------|-----------------|----------|--------------------------------------------------|
| dhtmlxTreeGrid                                     | GNU - noTodo    |          | https://dhtmlx.com/docs/products/dhtmlxTreeGrid  |
| DHTMLX Gantt                                       | GNU 100%        |          | https://github.com/DHTMLX/gantt                  |
| TreeGrid                                           | GNU - noTodo    |          | http://www.treegrid.com/Grid                     |
| SlickGrid                                          | GNU 100%        |          | https://github.com/6pac/SlickGrid                |

