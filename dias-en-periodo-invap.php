<?PHP 

$file = "data/periodos-invap.json";
$string = file_get_contents($file);
$json_a = json_decode($string, true);

#echo print_r($json_a,true);
$pi = $json_a["pi"];
#echo print_r($pi,true);
foreach( $pi as $k => $v )
   {
   #echo print_r($v,true);
   $datetime1 = date_create($v["date_start"]);
   $datetime2 = date_create($v["date_end"]);
   $interval = date_diff($datetime1, $datetime2);
   $inter=$interval->format('%R%a días');
   printf("%10s | %10s | %10s | %10s\n", $k , $v["date_start"], $v["date_end"], $inter) ;
   }
