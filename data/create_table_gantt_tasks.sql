CREATE TABLE gantt_tasks (
  id             integer NOT NULL,
  text           varchar NULL,
  start_date     date NOT NULL,
  duration       integer NOT NULL,
  progress       float NOT NULL ,
  sortorder      integer NOT NULL,
  parent         integer NOT NULL

);
ALTER TABLE gantt_tasks ADD CONSTRAINT PK_primary
	PRIMARY KEY (id)
;
