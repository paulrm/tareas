--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: gantt_tasks; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE gantt_links (
    id integer NOT NULL,
    source integer NOT NULL,
    target integer NOT NULL,
    type varchar 
);


ALTER TABLE public.gantt_links OWNER TO "planAdmin";

--
-- Name: pk_primary; Type: CONSTRAINT; Schema: public; Owner: planAdmin; Tablespace: 
--

ALTER TABLE ONLY gantt_links
    ADD CONSTRAINT pk_primary PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--
