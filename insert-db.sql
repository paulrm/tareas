--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: baseline; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE baseline (
    id_baseline bigint NOT NULL,
    version character varying,
    fecha date
);


ALTER TABLE public.baseline OWNER TO "planAdmin";

--
-- Name: categoria; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE categoria (
    id_categoria integer NOT NULL,
    nombre_categoria character varying,
    legajo character varying NOT NULL
);


ALTER TABLE public.categoria OWNER TO "planAdmin";

--
-- Name: categoria_tarea; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE categoria_tarea (
    id_tarea bigint NOT NULL
);


ALTER TABLE public.categoria_tarea OWNER TO "planAdmin";

--
-- Name: entregable; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE entregable (
    id_entregable character varying NOT NULL,
    ot character varying(50)
);


ALTER TABLE public.entregable OWNER TO "planAdmin";

--
-- Name: gantt_links; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE gantt_links (
    id integer NOT NULL,
    source integer NOT NULL,
    target integer NOT NULL,
    type character varying
);


ALTER TABLE public.gantt_links OWNER TO "planAdmin";

--
-- Name: gantt_tasks; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE gantt_tasks (
    id integer NOT NULL,
    text character varying,
    start_date date NOT NULL,
    duration integer NOT NULL,
    progress double precision NOT NULL,
    sortorder integer NOT NULL,
    parent integer NOT NULL
);


ALTER TABLE public.gantt_tasks OWNER TO "planAdmin";

--
-- Name: grupo; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE grupo (
    id_grupo integer NOT NULL,
    nombre_grupo character varying,
    responsable_coordinador character varying,
    pid_group integer
);


ALTER TABLE public.grupo OWNER TO "planAdmin";

--
-- Name: joinentregabletotarea; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE joinentregabletotarea (
    id_tarea bigint,
    id_entregable character varying
);


ALTER TABLE public.joinentregabletotarea OWNER TO "planAdmin";

--
-- Name: nivel_jerarquico; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE nivel_jerarquico (
    id_nivel_jerarquico integer NOT NULL,
    nombre_nivel_jerarquico character varying,
    legajo character varying NOT NULL
);


ALTER TABLE public.nivel_jerarquico OWNER TO "planAdmin";

--
-- Name: permisos; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE permisos (
    id_usuario bigint NOT NULL,
    id_grupo integer NOT NULL,
    id_rol integer NOT NULL
);


ALTER TABLE public.permisos OWNER TO "planAdmin";

--
-- Name: persona; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE persona (
    legajo character varying NOT NULL,
    nombre character varying,
    dni character varying,
    cc_grupo character varying,
    id_categoria integer,
    id_grupo integer,
    id_nivel_jerarquico integer,
    id_usuario bigint
);


ALTER TABLE public.persona OWNER TO "planAdmin";

--
-- Name: planificacion; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE planificacion (
    tipo character varying,
    fcha_inicio date,
    fcha_fin date,
    duracion_hs numeric,
    probabilidad numeric,
    id_grupo integer NOT NULL,
    id_tarea bigint NOT NULL,
    legajo character varying NOT NULL,
    id_baseline bigint NOT NULL
);


ALTER TABLE public.planificacion OWNER TO "planAdmin";

--
-- Name: rel_tarea_tipotarea; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE rel_tarea_tipotarea (
    tiempo_duracion numeric,
    id_tipo_tarea character varying NOT NULL,
    id_tarea bigint NOT NULL
);


ALTER TABLE public.rel_tarea_tipotarea OWNER TO "planAdmin";

--
-- Name: rol; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE rol (
    id_rol integer NOT NULL,
    nombre_rol character varying
);


ALTER TABLE public.rol OWNER TO "planAdmin";

--
-- Name: tarea; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE tarea (
    id_tarea bigint NOT NULL,
    nombre_tarea character varying,
    cc_pt character varying,
    ot character varying
);


ALTER TABLE public.tarea OWNER TO "planAdmin";

--
-- Name: tipo_tarea; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE tipo_tarea (
    id_tipo_tarea character varying NOT NULL,
    nombre_tarea character varying
);


ALTER TABLE public.tipo_tarea OWNER TO "planAdmin";

--
-- Name: tmp_periodos_invap; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE tmp_periodos_invap (
    id integer NOT NULL,
    desde date,
    hasta date,
    nombre character varying(20)
);


ALTER TABLE public.tmp_periodos_invap OWNER TO "planAdmin";

--
-- Name: tmp_periodos_invap_id_seq; Type: SEQUENCE; Schema: public; Owner: planAdmin
--

CREATE SEQUENCE tmp_periodos_invap_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tmp_periodos_invap_id_seq OWNER TO "planAdmin";

--
-- Name: tmp_periodos_invap_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: planAdmin
--

ALTER SEQUENCE tmp_periodos_invap_id_seq OWNED BY tmp_periodos_invap.id;


--
-- Name: usuario; Type: TABLE; Schema: public; Owner: planAdmin; Tablespace: 
--

CREATE TABLE usuario (
    id_usuario bigint NOT NULL,
    nombre_usuario character varying,
    contrasenia character varying
);


ALTER TABLE public.usuario OWNER TO "planAdmin";

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: planAdmin
--

ALTER TABLE ONLY tmp_periodos_invap ALTER COLUMN id SET DEFAULT nextval('tmp_periodos_invap_id_seq'::regclass);


--
-- Data for Name: baseline; Type: TABLE DATA; Schema: public; Owner: planAdmin
--

COPY baseline (id_baseline, version, fecha) FROM stdin;
1	1	2018-02-07
\.


--
-- Data for Name: categoria; Type: TABLE DATA; Schema: public; Owner: planAdmin
--

COPY categoria (id_categoria, nombre_categoria, legajo) FROM stdin;
\.


--
-- Data for Name: categoria_tarea; Type: TABLE DATA; Schema: public; Owner: planAdmin
--

COPY categoria_tarea (id_tarea) FROM stdin;
1
2
3
4
\.


--
-- Data for Name: entregable; Type: TABLE DATA; Schema: public; Owner: planAdmin
--

COPY entregable (id_entregable, ot) FROM stdin;
\.


--
-- Data for Name: gantt_links; Type: TABLE DATA; Schema: public; Owner: planAdmin
--

COPY gantt_links (id, source, target, type) FROM stdin;
\.


--
-- Data for Name: gantt_tasks; Type: TABLE DATA; Schema: public; Owner: planAdmin
--

COPY gantt_tasks (id, text, start_date, duration, progress, sortorder, parent) FROM stdin;
1	TareaTest01	2018-02-01	5	0	0	0
2	TareaTask03	2018-02-01	0	0	0	0
\.


--
-- Data for Name: grupo; Type: TABLE DATA; Schema: public; Owner: planAdmin
--

COPY grupo (id_grupo, nombre_grupo, responsable_coordinador, pid_group) FROM stdin;
1	QA EISA_G	Moncaglieri	11
2	QA EISA_E	Moncaglieri	11
3	Depósito Mendoza	Oses	11
4	PE Mendoza	Ugarte	11
5	QC Mendoza	Danta	11
6	PEM EICO	Barriga	11
7	QC EICO	Danta	11
8	IyE EICO	Malleret	11
9	PH Moreno	Neira	11
10	QC Moreno	Malleret	11
12	CC254	Conti	\N
11	MAIT	Malleret	12
13	Gestor	Franco	\N
\.


--
-- Data for Name: joinentregabletotarea; Type: TABLE DATA; Schema: public; Owner: planAdmin
--

COPY joinentregabletotarea (id_tarea, id_entregable) FROM stdin;
\.


--
-- Data for Name: nivel_jerarquico; Type: TABLE DATA; Schema: public; Owner: planAdmin
--

COPY nivel_jerarquico (id_nivel_jerarquico, nombre_nivel_jerarquico, legajo) FROM stdin;
100	Jefe_CC	100
101	Coordinador	101
102	Supervisor	102
103	Armador	103
\.


--
-- Data for Name: permisos; Type: TABLE DATA; Schema: public; Owner: planAdmin
--

COPY permisos (id_usuario, id_grupo, id_rol) FROM stdin;
1	13	2
2	11	1
3	12	2
4	8	1
5	10	1
6	1	1
\.


--
-- Data for Name: persona; Type: TABLE DATA; Schema: public; Owner: planAdmin
--

COPY persona (legajo, nombre, dni, cc_grupo, id_categoria, id_grupo, id_nivel_jerarquico, id_usuario) FROM stdin;
3838	Cuadrado Daiana		Gestor	3	13	4	1
3001	Villarroel Romina		QA EISA_E	1	2	1	17
3002	Correa Marcelo		PE Mendoza	1	4	1	13
3003	Malleret		MAIT	1	11	1	2
3004	Conti Javier		CC254	1	12	1	3
3005	Danta Adrian		QC Mendoza	1	5	1	4
\.


--
-- Data for Name: planificacion; Type: TABLE DATA; Schema: public; Owner: planAdmin
--

COPY planificacion (tipo, fcha_inicio, fcha_fin, duracion_hs, probabilidad, id_grupo, id_tarea, legajo, id_baseline) FROM stdin;
test	2018-02-10	2018-06-10	200	75	7	1	3001	1
\.


--
-- Data for Name: rel_tarea_tipotarea; Type: TABLE DATA; Schema: public; Owner: planAdmin
--

COPY rel_tarea_tipotarea (tiempo_duracion, id_tipo_tarea, id_tarea) FROM stdin;
\.


--
-- Data for Name: rol; Type: TABLE DATA; Schema: public; Owner: planAdmin
--

COPY rol (id_rol, nombre_rol) FROM stdin;
1	Planificador
2	Visualizador
\.


--
-- Data for Name: tarea; Type: TABLE DATA; Schema: public; Owner: planAdmin
--

COPY tarea (id_tarea, nombre_tarea, cc_pt, ot) FROM stdin;
1	Diseño SAOCOM	0804	
2	Fabricación SAOCOM	0804	
3	Diseño RPA	0874	
4	Fabricación RPA	0874	
5	Diseño SARA	0888	
6	Fabricación SARA	0888	
7	PEM SABIAMAR	0965	1111
\.


--
-- Data for Name: tipo_tarea; Type: TABLE DATA; Schema: public; Owner: planAdmin
--

COPY tipo_tarea (id_tipo_tarea, nombre_tarea) FROM stdin;
\.


--
-- Data for Name: tmp_periodos_invap; Type: TABLE DATA; Schema: public; Owner: planAdmin
--

COPY tmp_periodos_invap (id, desde, hasta, nombre) FROM stdin;
1	2017-10-16	2017-11-15	Oct/Nov-2017
2	2017-11-16	2017-12-04	Nov/Dic-2017
3	2017-12-05	2018-01-15	Dic/Ene-2018
4	2018-01-16	2018-02-15	Ene/Feb-2018
5	2018-02-16	2018-03-15	Feb/Mar-2018
6	2018-03-16	2018-04-15	Mar/Abr-2018
7	2018-04-16	2018-05-15	Abr/May-2018
8	2018-05-16	2018-06-15	May/Jun-2018
9	2018-06-16	2018-07-15	Jun/Jul-2018
10	2018-07-16	2018-08-15	Jul/Ago-2018
11	2018-08-16	2018-09-15	Ago/Sep-2018
12	2018-09-16	2018-10-15	Sep/Oct-2018
13	2018-10-16	2018-11-15	Oct/Nov-2018
\.


--
-- Name: tmp_periodos_invap_id_seq; Type: SEQUENCE SET; Schema: public; Owner: planAdmin
--

SELECT pg_catalog.setval('tmp_periodos_invap_id_seq', 12, true);


--
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: planAdmin
--

COPY usuario (id_usuario, nombre_usuario, contrasenia) FROM stdin;
2	Malleret	rmalleret
3	Conti	jconti
4	Danta	adanta
1	Cuadrado	dcuadrado
5	Neira	cneira
6	Moncaglieri	rmoncaglieri
7	Bello	rbello
8	Margarido	mmargarido
9	Ortiz	lortiz
10	Sarasqueta	jsarasqueta
11	Valverde	mvalverde
12	Guerrero Lucas	lguerrero
13	Correa	mcorrea
14	Soto	nsoto
15	Brocca	lbrocca
16	Jocano	ajocano
17	Villarroel 	rvillarroel
\.


--
-- Name: pk_baseline; Type: CONSTRAINT; Schema: public; Owner: planAdmin; Tablespace: 
--

ALTER TABLE ONLY baseline
    ADD CONSTRAINT pk_baseline PRIMARY KEY (id_baseline);


--
-- Name: pk_categoria; Type: CONSTRAINT; Schema: public; Owner: planAdmin; Tablespace: 
--

ALTER TABLE ONLY categoria
    ADD CONSTRAINT pk_categoria PRIMARY KEY (legajo, id_categoria);


--
-- Name: pk_categoria_tarea; Type: CONSTRAINT; Schema: public; Owner: planAdmin; Tablespace: 
--

ALTER TABLE ONLY categoria_tarea
    ADD CONSTRAINT pk_categoria_tarea PRIMARY KEY (id_tarea);


--
-- Name: pk_entregable; Type: CONSTRAINT; Schema: public; Owner: planAdmin; Tablespace: 
--

ALTER TABLE ONLY entregable
    ADD CONSTRAINT pk_entregable PRIMARY KEY (id_entregable);


--
-- Name: pk_grupo; Type: CONSTRAINT; Schema: public; Owner: planAdmin; Tablespace: 
--

ALTER TABLE ONLY grupo
    ADD CONSTRAINT pk_grupo PRIMARY KEY (id_grupo);


--
-- Name: pk_nivel_jerarquico; Type: CONSTRAINT; Schema: public; Owner: planAdmin; Tablespace: 
--

ALTER TABLE ONLY nivel_jerarquico
    ADD CONSTRAINT pk_nivel_jerarquico PRIMARY KEY (legajo, id_nivel_jerarquico);


--
-- Name: pk_permisos; Type: CONSTRAINT; Schema: public; Owner: planAdmin; Tablespace: 
--

ALTER TABLE ONLY permisos
    ADD CONSTRAINT pk_permisos PRIMARY KEY (id_usuario, id_grupo, id_rol);


--
-- Name: pk_persona; Type: CONSTRAINT; Schema: public; Owner: planAdmin; Tablespace: 
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT pk_persona PRIMARY KEY (legajo);


--
-- Name: pk_planificacion; Type: CONSTRAINT; Schema: public; Owner: planAdmin; Tablespace: 
--

ALTER TABLE ONLY planificacion
    ADD CONSTRAINT pk_planificacion PRIMARY KEY (id_grupo, id_tarea, legajo, id_baseline);


--
-- Name: pk_primary; Type: CONSTRAINT; Schema: public; Owner: planAdmin; Tablespace: 
--

ALTER TABLE ONLY gantt_tasks
    ADD CONSTRAINT pk_primary PRIMARY KEY (id);


--
-- Name: pk_primary_links; Type: CONSTRAINT; Schema: public; Owner: planAdmin; Tablespace: 
--

ALTER TABLE ONLY gantt_links
    ADD CONSTRAINT pk_primary_links PRIMARY KEY (id);


--
-- Name: pk_rel_tarea_tipotarea; Type: CONSTRAINT; Schema: public; Owner: planAdmin; Tablespace: 
--

ALTER TABLE ONLY rel_tarea_tipotarea
    ADD CONSTRAINT pk_rel_tarea_tipotarea PRIMARY KEY (id_tipo_tarea, id_tarea);


--
-- Name: pk_rol; Type: CONSTRAINT; Schema: public; Owner: planAdmin; Tablespace: 
--

ALTER TABLE ONLY rol
    ADD CONSTRAINT pk_rol PRIMARY KEY (id_rol);


--
-- Name: pk_tarea; Type: CONSTRAINT; Schema: public; Owner: planAdmin; Tablespace: 
--

ALTER TABLE ONLY tarea
    ADD CONSTRAINT pk_tarea PRIMARY KEY (id_tarea);


--
-- Name: pk_tipo_tarea; Type: CONSTRAINT; Schema: public; Owner: planAdmin; Tablespace: 
--

ALTER TABLE ONLY tipo_tarea
    ADD CONSTRAINT pk_tipo_tarea PRIMARY KEY (id_tipo_tarea);


--
-- Name: pk_usuario; Type: CONSTRAINT; Schema: public; Owner: planAdmin; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT pk_usuario PRIMARY KEY (id_usuario);


--
-- Name: tmp_periodos_invap_pkey; Type: CONSTRAINT; Schema: public; Owner: planAdmin; Tablespace: 
--

ALTER TABLE ONLY tmp_periodos_invap
    ADD CONSTRAINT tmp_periodos_invap_pkey PRIMARY KEY (id);


--
-- Name: fk_baseline_planificacion; Type: FK CONSTRAINT; Schema: public; Owner: planAdmin
--

ALTER TABLE ONLY planificacion
    ADD CONSTRAINT fk_baseline_planificacion FOREIGN KEY (id_baseline) REFERENCES baseline(id_baseline);


--
-- Name: fk_compuesto_por_grupo; Type: FK CONSTRAINT; Schema: public; Owner: planAdmin
--

ALTER TABLE ONLY grupo
    ADD CONSTRAINT fk_compuesto_por_grupo FOREIGN KEY (id_grupo) REFERENCES grupo(id_grupo);


--
-- Name: fk_coordina_persona; Type: FK CONSTRAINT; Schema: public; Owner: planAdmin
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT fk_coordina_persona FOREIGN KEY (legajo) REFERENCES persona(legajo);


--
-- Name: fk_entregable; Type: FK CONSTRAINT; Schema: public; Owner: planAdmin
--

ALTER TABLE ONLY joinentregabletotarea
    ADD CONSTRAINT fk_entregable FOREIGN KEY (id_entregable) REFERENCES entregable(id_entregable);


--
-- Name: fk_es_usuario; Type: FK CONSTRAINT; Schema: public; Owner: planAdmin
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT fk_es_usuario FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario);


--
-- Name: fk_grupo_permisos; Type: FK CONSTRAINT; Schema: public; Owner: planAdmin
--

ALTER TABLE ONLY permisos
    ADD CONSTRAINT fk_grupo_permisos FOREIGN KEY (id_grupo) REFERENCES grupo(id_grupo);


--
-- Name: fk_grupo_planificacion; Type: FK CONSTRAINT; Schema: public; Owner: planAdmin
--

ALTER TABLE ONLY planificacion
    ADD CONSTRAINT fk_grupo_planificacion FOREIGN KEY (id_grupo) REFERENCES grupo(id_grupo);


--
-- Name: fk_persona_planificacion; Type: FK CONSTRAINT; Schema: public; Owner: planAdmin
--

ALTER TABLE ONLY planificacion
    ADD CONSTRAINT fk_persona_planificacion FOREIGN KEY (legajo) REFERENCES persona(legajo);


--
-- Name: fk_pertenece_persona; Type: FK CONSTRAINT; Schema: public; Owner: planAdmin
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT fk_pertenece_persona FOREIGN KEY (id_grupo) REFERENCES grupo(id_grupo);


--
-- Name: fk_rol_permisos; Type: FK CONSTRAINT; Schema: public; Owner: planAdmin
--

ALTER TABLE ONLY permisos
    ADD CONSTRAINT fk_rol_permisos FOREIGN KEY (id_rol) REFERENCES rol(id_rol);


--
-- Name: fk_subordina_tarea; Type: FK CONSTRAINT; Schema: public; Owner: planAdmin
--

ALTER TABLE ONLY tarea
    ADD CONSTRAINT fk_subordina_tarea FOREIGN KEY (id_tarea) REFERENCES tarea(id_tarea);


--
-- Name: fk_tarea; Type: FK CONSTRAINT; Schema: public; Owner: planAdmin
--

ALTER TABLE ONLY joinentregabletotarea
    ADD CONSTRAINT fk_tarea FOREIGN KEY (id_tarea) REFERENCES tarea(id_tarea);


--
-- Name: fk_tarea_depende; Type: FK CONSTRAINT; Schema: public; Owner: planAdmin
--

ALTER TABLE ONLY rel_tarea_tipotarea
    ADD CONSTRAINT fk_tarea_depende FOREIGN KEY (id_tarea) REFERENCES tarea(id_tarea);


--
-- Name: fk_tarea_planificacion; Type: FK CONSTRAINT; Schema: public; Owner: planAdmin
--

ALTER TABLE ONLY planificacion
    ADD CONSTRAINT fk_tarea_planificacion FOREIGN KEY (id_tarea) REFERENCES tarea(id_tarea);


--
-- Name: fk_tipo_tarea_depende; Type: FK CONSTRAINT; Schema: public; Owner: planAdmin
--

ALTER TABLE ONLY rel_tarea_tipotarea
    ADD CONSTRAINT fk_tipo_tarea_depende FOREIGN KEY (id_tipo_tarea) REFERENCES tipo_tarea(id_tipo_tarea);


--
-- Name: fk_usuario_permisos; Type: FK CONSTRAINT; Schema: public; Owner: planAdmin
--

ALTER TABLE ONLY permisos
    ADD CONSTRAINT fk_usuario_permisos FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

