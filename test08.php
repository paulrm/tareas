<!DOCTYPE html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<title>INVAP Planner</title>
	<script src="js/dhtmlxgantt.js" type="text/javascript" charset="utf-8"></script>
	<script src="js/dhtmlxgantt_multiselect.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="css/dhtmlxgantt.css" type="text/css" media="screen" title="no title" charset="utf-8">

	<script src="js/jquery-1.11.1.min.js" type="text/javascript" charset="utf-8"></script>

	<link rel="stylesheet" href="bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<script src="bootstrap/3.2.0/js/bootstrap.min.js"></script>

	<style type="text/css">
		html, body{ height:100%; padding:0px; margin:0px; }
		.weekend{ background: #f4f7f4 !important;}
		.gantt_selected .weekend{ background:#FFF3A1 !important; }
		.well {
			text-align: right;
		}
		@media (max-width: 991px) {
			.nav-stacked>li{ float: left;}
		}
		.container-fluid .row {
			margin-bottom: 10px;
		}
		.container-fluid .gantt_wrapper {
			height: 700px;
			width: 100%;
		}
		.gantt_container {
			border-radius: 4px;
		}
		.gantt_grid_scale { background-color: transparent; }
		.gantt_hor_scroll { margin-bottom: 1px; }
	</style>
<script>
function testMSG(){
msgbox("test");
}
</script>


</head>
<body>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="navbar navbar-inverse">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">INVAP Planner</a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Project <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Project information</a></li>
									<li><a href="#">Custom fields</a></li>
									<li><a href="#">Change working time</a></li>
									<li class="divider"></li>
									<li><a href="#">Export</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Task <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#" onclick="indent();return false;">Indent</a></li>
									<li><a href="#" onclick="outdent();return false;">Outdent</a></li>
									<li class="divider"></li>
									<li><a href="#" onclick="testMsg();return false;">Summary</a></li>
								</ul>
							</li>
							<li><a href="#">Team</a></li>
							<li><a href="#">Format</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	<!--	<div class="col-md-2 col-md-push-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Gantt info</h3>
				</div>
				<div class="panel-body">
					<ul class="nav nav-pills nav-stacked" id="gantt_info">
					</ul>
				</div>
			</div>
		</div>
        -->
		<div >
			<div class="gantt_wrapper panel" id="gantt_here"></div>
		</div>
	</div>
</div>

<script type="text/javascript">


	// indent-outdent implementation
	//(function(){

		function shiftTask(task_id, direction) {
			var task = gantt.getTask(task_id);
			task.start_date = gantt.date.add(task.start_date, direction, "day");
			task.end_date = gantt.calculateEndDate(task.start_date, task.duration);
			gantt.updateTask(task.id);
		}

		var actions = {
			"indent": function indent(task_id){
				var prev_id = gantt.getPrevSibling(task_id);
				while(gantt.isSelectedTask(prev_id)){
					var prev = gantt.getPrevSibling(prev_id);
					if(!prev) break;
					prev_id = prev;
				}
				if (prev_id) {
					var new_parent = gantt.getTask(prev_id);
					gantt.moveTask(task_id, gantt.getChildren(new_parent.id).length, new_parent.id);
					new_parent.type = gantt.config.types.project;
					new_parent.$open = true;
					gantt.updateTask(task_id);
					gantt.updateTask(new_parent.id);
					return task_id;
				}
				return null;
			},
			"outdent": function outdent(task_id){
				var cur_task = gantt.getTask(task_id);
				var old_parent = cur_task.parent;
				if (gantt.isTaskExists(old_parent) && old_parent != gantt.config.root_id){
					gantt.moveTask(task_id, gantt.getTaskIndex(old_parent)+1+gantt.getTaskIndex(task_id), gantt.getParent(cur_task.parent));
					if (!gantt.hasChild(old_parent))
						gantt.getTask(old_parent).type = gantt.config.types.task;
					gantt.updateTask(task_id);
					gantt.updateTask(old_parent);
					return task_id;
				}
				return null;
			},
			"del": function(task_id){
				gantt.deleteTask(task_id);
				return task_id;
			},
			"moveForward": function(task_id){
				shiftTask(task_id, 1);
			},
			"moveBackward": function(task_id){
				shiftTask(task_id, -1);
			}
		};
		var cascadeAction = {
			"indent":true,
			"outdent":true,
			"del":true
		};

		gantt.performAction = function(actionName){
			var action = actions[actionName];
			if(!action)
				return;

			gantt.batchUpdate(function () {
				var updated = {};
				gantt.eachSelectedTask(function(task_id){

					if(cascadeAction[actionName]){
						if(!updated[gantt.getParent(task_id)]){
							var updated_id = action(task_id);
							updated[updated_id] = true;
						}else{
							updated[task_id] = true;
						}
					}else{
						action(task_id);
					}
				});
			});
		};


//	})();



	var getListItemHTML = function (type, count, active) {
		return '<li'+(active?' class="active"':'')+'><a href="#">'+type+'s <span class="badge">'+count+'</span></a></li>';
	};

	var updateInfo = function () {
		var state = gantt.getState(),
				tasks = gantt.getTaskByTime(state.min_date, state.max_date),
				types = gantt.config.types,
				result = {},
				html = "",
				active = false;

		// get available types
		for (var t in types) {
			result[types[t]] = 0;
		}
		// sort tasks by type
		for (var i=0, l=tasks.length; i<l; i++) {
			if (tasks[i].type && result[tasks[i].type] != "undefined")
				result[tasks[i].type] += 1;
			else
				result[types.task] += 1;
		}
		// render list items for each type
		for (var j in result) {
			if (j == types.task)
				active = true;
			else
				active = false;
			html += getListItemHTML(j, result[j], active);
		}

	};

	gantt.templates.scale_cell_class = function(date){
		if(date.getDay()==0||date.getDay()==6){
			return "weekend";
		}
	};
	gantt.templates.task_cell_class = function(item,date){
		if(date.getDay()==0||date.getDay()==6){
			return "weekend" ;
		}
	};

	gantt.templates.rightside_text = function(start, end, task){
		if(task.type == gantt.config.types.milestone){
			return task.text;
		}
		return "";
	};

	gantt.config.columns = [
		{name:"text",       label:"Task name",  width:"*", tree:true },
		{name:"duration",   label:"Duration", align:"center", width:60},
        {name:"responsable", label:"responsable", align: "center", width:100,
            template: function(item) {
                if (!item.users) return "Nobody";
                return item.users.join(", ");
            }
        },
        {name:"ejecutor", label:"ejecutor", align: "center", width:100,
            template: function(item) {
                if (!item.users) return "Nobody";
                return item.users.join(", ");
            }
        },
		{name:"add",        label:"",           width:44 }
	];

	gantt.config.grid_width = 390;
	gantt.config.date_grid = "%F %d";
	gantt.config.scale_height  = 60;
	gantt.config.subscales = [
		{ unit:"week", step:1, date:"Week #%W"}
	];

	gantt.attachEvent("onAfterTaskAdd", function(id,item){
		updateInfo();
	});
	gantt.attachEvent("onAfterTaskDelete", function(id,item){
		updateInfo();
	});

gantt.config.xml_date = "%Y-%m-%d %H:%i:%s";
	gantt.init("gantt_here");
        gantt.load("http://hal-api-rest.invap.com.ar/data");
	updateInfo();

        var dp = new gantt.dataProcessor("http://hal-api-rest.invap.com.ar/data");
        dp.init(gantt);
        dp.setTransactionMode("REST");


function indent(){
gantt.performAction("indent");
}
function outdent(){
gantt.performAction("outdent");
}
</script>
</body>
